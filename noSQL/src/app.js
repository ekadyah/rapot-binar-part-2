const express = require('express');
const bodyParser=require('body-parser')
const routes = require('./routes');

module.exports = function mainApp(port) {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(routes());
    app.listen(port, () => console.log(`this app run at port:${port}`))
};
