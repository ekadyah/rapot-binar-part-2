const router = require('express').Router();
const userController = require('../controller/usersController')();

module.exports = function routes() {   
        
    router.get('/',(req, res)=> res.send("<h1>This is homepage</h1>"));
    
    router.use('/api/users', userController);

    return router
}




