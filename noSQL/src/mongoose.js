const mongoose = require('mongoose');
require('dotenv').config()
const port = process.env.PORT || 5500;
// const dbData = {
//     username: process.env.DB_USERNAME,
//     password:process.env.DB_PASSWORD,
//     cluster:process.env.DB_CLUSTER,
//     database:process.env.DB_DATABASE
// }
module.exports = async function mongoConnect() {
    await mongoose.connect(process.env.DATABASE,
        {       
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true,
        useCreateIndex:true
    })
}

