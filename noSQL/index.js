const app = require('./src/app');
const mongoose = require('mongoose');
const router = require('express').Router();

require('dotenv').config();
const express = require('express');
const bodyParser=require('body-parser')
// const routes = require('./routes');
const userController = require('./controller/usersController')();
const profileController = require('./controller/profileController')

module.exports = function mainApp(port) {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(routes());
    app.listen(port, () => console.log(`this app run at port:${port}`))
};

(async () => {
  try {
    await mongoConnect();
    app(process.env.PORT||5000);
  } catch (error) {
    console.log(error);
      console.log('error:app cannot running')
  }
})()
