const { Router } = require("express");
const control = require('express').Router();
const usersModel = require('../model/userModel')();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const loginAuth = require('../middleware/loginAuth');


module.exports=function userController() {        

    control.get('/user', loginAuth, async (req, res) => {
        const id = res.locals.user.id;
        const data=await (await usersModel.findOne({_id:id})).populate("profile");
        res.json({ message: "success read data", data:data })
    })

    control.post('/register', async (req, res) => {
               
        const payload = req.body;
        const password=payload.password

        const saltKey = await bcrypt.genSalt(10);
        const hashpassword = await bcrypt.hash(password,saltKey);
        
            const data = {
                username: payload.username,
                email: payload.email,
                password: hashpassword,
                salt: saltKey,
            }       
        
            await usersModel.create(data)
            
            res.json({message:"success create data user"})
    })

    control.post('/login', loginAuth, async (req, res) => {
        let statusCode = 500;
        try {
            const username = req.body.username;
            const password= req.body.password;
            const users = await usersModel.findOne({
                username:username
            })

            if (!users) {
                statusCode= 404;
                throw new Error('Users not found')
            }
            const isPasswordMatch = await bcrypt.compare(password, users.password);
            if (!isPasswordMatch) {
                statusCode = 400;
                throw new Error('password invalid')
            }
            
            const token = jwt.sign({
                id:users._id,username: users.username
            },process.env.SALT_KEY)

            res.json({
                message: "success login",
                data: {
                    token: token
                }
            });

        } catch (error) {
            res.status(statusCode).json({
                message:error.message
            })
        }
    })

    control.put("/update", loginAuth, async (req, res) => {
        await user.updateOne({_id: id});
        res.json({ message: "success update data" });
      });
      
      control.delete("/delete", loginAuth, async (req, res) => {
        const id = user.deleteOne({ _id: id });
        res.json({ message: "success deleted data", id: id });
      });
      

    return control
}