const usersModel = require("../model/userModel");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
require('dotenv').config();
module.exports = function loginAuth(req, res, next) {
    
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decToken = jwt.verify(token, process.env.SALT_KEY);
        res.locals.user = decToken  

        next();
    } catch (error) {
        res.status(400).json({message:"token invalid"})
    }
  
}
