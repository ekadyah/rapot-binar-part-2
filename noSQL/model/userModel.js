const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// require("dotenv").config();


mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  
module.exports = function usersModel() {
    const userSchema = new Schema({
        username: String,
        email: String,
        password: String,
        salt:String,
        profile: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "profile",
        },
    });
  
    return mongoose.model("user", userSchema);
  };
  