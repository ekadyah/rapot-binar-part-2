const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// require("dotenv").config();


mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  

module.exports = function usersModel() {
    const userSchema = new Schema({
        firstName: String,
        lastName: String,
        gender: String,
        }),
  
    return mongoose.model("users", userSchema);
  };
  