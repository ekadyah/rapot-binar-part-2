const usersController = require("./userController");
const tasksController = require("./profileController");

class Controller {
  users() {
    return userController;
  }
  profile() {
    return profileController;
  }
}

module.exports = Object.freeze(new Controller());
