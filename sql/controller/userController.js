const express = require("express");
const db = require("./models");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const secretKey = process.env.SECRET_KEY;

const app = express();

const routes = require("./routes");

app.use(bodyParser.json());

app.use("/", routes);

const middlewareAuth = (req, res, next) => {
    try {
      const token = req.headers.authorization.split(" ")[1];
      jwt.verify(token, "r4potb1nar");
  
      next();
    } catch (error) {
      res.status(400).json({ message: "token invalid" });
    }
  };
  
app.post("/api/register", async (req, res) => {
  try {
    const payload = req.body;
    const username = payload.username;
    const password = payload.password;
    const saltkey = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, saltkey);

    const data = await db.users.create({
      username: username,
      password: hashPassword,
      salt: saltkey,
    });

    res.json({ message: "success created user", data: data });
  } catch (error) {
    res.json({ message: error.message });
  }
});

app.post("/api/login", async (req, res) => {
  try {
    const payload = req.body;
    const username = payload.username;
    const password = payload.password;

    const users = await db.users.findOne({ where: { username: username } });

    if (!users) {
      status = 404;
      throw new Error("user not found");
    }

    const isPasswordMatch = await bcrypt.compare(password, users.password);
    if (!isPasswordMatch) {
      statusCode = 400;
      throw new Error("password invalid");
    }

    const token = jwt.sign({ username: users.username }, "r4potb1nar");

    res.json({ message: "login successfull", data: { token: token } });
  } catch (error) {
    res.json({ message: error.message });
  }
});


app.get("/api/users", middlewareAuth, async (req, res) => {
  const data = await db.users.findAll({
      include: [{model: db.profile, as: "profile",},],
  });
  res.json({ message: "success read data", data: data });
});

app.put("/api/users", middlewareAuth, async (req, res) => {
  await db.users.update(
    {
    
      username: req.body.username,
    },
    { where: { id: req.body.id } }
  );
  res.json({ message: "success update data" });
});

app.delete("/api/users", middlewareAuth, async (req, res) => {
  const id = await db.users.destroy({ where: { id: req.query.id } });
  res.json({ message: "success deleted data", id: id });
});



app.listen(5200, () => console.log("this app running on port 5200"));
