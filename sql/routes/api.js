const router = require("express").Router();

// const controller = require("../controllers/");

router.post("/users", controller.user().post);

// Read users
router.get("/users", controller.user().get);

// Update users
router.put("/users", controller.user().put);

// Delete users
router.delete("/users", controller.users().delete);
// // Routing tasks
module.exports = router;
