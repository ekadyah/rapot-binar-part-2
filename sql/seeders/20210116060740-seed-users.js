"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     */
    await queryInterface.bulkInsert(
      "users",
      [
        {
          username: "Ekaeka",
          password: "eka123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "Kemahkemah",
          password: "kemah123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "Nurnur",
          password: "nur123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     */
    await queryInterface.bulkDelete("users", null, {});
  },
};
